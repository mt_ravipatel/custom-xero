<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Xero_connection extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        require __DIR__ . '/third_party/xero/vendor/autoload.php';
        require_once(__DIR__.'/xero_storage.php');
        
        @session_start();
    }

    public function index() {
        ini_set('display_errors', 'On');
          // Storage Class uses sessions for storing access token (demo only)
          // you'll need to extend to your Database for a scalable solution

         


          $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->config->item('clientId'),   
            'clientSecret'            => $this->config->item('clientSecret'),
            'redirectUri'             => $this->config->item('redirectUri'),
            'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken'          => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
          ]);

          // Scope defines the data your app has permission to access.
          // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
          $options = [
              'scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments']
          ];

          // This returns the authorizeUrl with necessary parameters applied (e.g. state).
          $authorizationUrl = $provider->getAuthorizationUrl($options);

          // Save the state generated for you and store it to the session.
          // For security, on callback we compare the saved state with the one returned to ensure they match.
          $_SESSION['oauth2state'] = $provider->getState();

          // Redirect the user to the authorization URL.
          header('Location: ' . $authorizationUrl);
          exit();
    }


    public function xero_callback(){

      $storage = new StorageClass();

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => $this->config->item('clientId'),   
          'clientSecret'            => $this->config->item('clientSecret'),
          'redirectUri'             => $this->config->item('redirectUri'),
          'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
          'urlAccessToken'          => 'https://identity.xero.com/connect/token',
          'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
         
        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
          echo "Something went wrong, no authorization code found";
          exit("Something went wrong, no authorization code found");

        // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
          echo "Invalid State";
          unset($_SESSION['oauth2state']);
          exit('Invalid state');
        } else {
        
          try {
            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', [
              'code' => $_GET['code']
            ]);
                 
            $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$accessToken->getToken() );
          
            $config->setHost("https://api.xero.com"); 
            $identityInstance = new XeroAPI\XeroPHP\Api\IdentityApi(
              new GuzzleHttp\Client(),
              $config
            );
             
            $result = $identityInstance->getConnections();

            // Save my tokens, expiration tenant_id
            $storage->setToken(
                $accessToken->getToken(),
                $accessToken->getExpires(),
                $result,
                $accessToken->getRefreshToken(),
                $accessToken->getValues()["id_token"]
            );

            echo('Connection ok');exit();
           
          } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            var_dump($e);
            echo($e->xdebug_message);
            echo "Callback failed";
            exit();
          }
        }
    }
    
}

?>