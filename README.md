

1 . Within application\config\constants.php add the following lines:

````
defined('VENDORPATH') OR define('VENDORPATH', FCPATH.'vendor');``
`````

2. Create Composer.json file to Project Root and  add the following lines
````
{
    "repositories": [
        {
            "type": "vcs",
            "url" : "https://mt_ravipatel@bitbucket.org/mt_ravipatel/xero_package.git"

        }
    ],
    "require": {
        "mpdf/mpdf": "^8.0.10",
        "mt_ravipatel/custom-xero": "dev-master"
    }
}
````

3. Create a file application\controllers\Xero_connection.php with the following content:

````
<?php
if (is_file(VENDORPATH.'\mt_ravipatel\custom-xero\Xero_connection.php')) {
    require_once VENDORPATH.'\mt_ravipatel\custom-xero\Xero_connection.php';
}
?>
````

4. Within  Existing autoload helper in  add the following lines:

````
<?php
if (is_file(VENDORPATH.'\mt_ravipatel\custom-xero\helpers\xero_helper.php')) {
    require_once VENDORPATH.'\mt_ravipatel\custom-xero\helpers\xero_helper.php';
}
?>
````

5. Create a file application\libraries\Xero.php with the following content:

````
<?php
if (is_file(VENDORPATH.'\mt_ravipatel\custom-xero\libraries\Xero.php')) {
    require_once VENDORPATH.'\mt_ravipatel\custom-xero\libraries\Xero.php';
}
?>
````


6. Within application\config\autoload.php add the following lines:

````
<?php
$autoload['libraries'] = array('xero');
?>
````